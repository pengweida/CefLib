#include "js_handler.h"
#include "ipc_string_define.h"
#include "cef_js_bridge.h"

namespace cef_render
{
  bool CefJSHandler::Execute(const CefString& name, CefRefPtr<CefV8Value> object, const CefV8ValueList& arguments, CefRefPtr<CefV8Value>& retval, CefString& exception)
  {
    CefRefPtr<CefV8Context> context = CefV8Context::GetCurrentContext();
    CefRefPtr<CefFrame> frame = context->GetFrame();
    CefRefPtr<CefBrowser> browser = context->GetBrowser();

    int64_t browser_id = browser->GetIdentifier();
    int64_t frame_id = frame->GetIdentifier();

    if (name == "call")
    {
      // 允许没有参数列表的调用，第二个参数为回调
      // 如果传递了参数列表，那么回调是第三个参数
      CefString function_name = arguments[0]->GetStringValue();
      CefString params = "{}";
      CefRefPtr<CefV8Value> callback;
      if (arguments[0]->IsString() && arguments[1]->IsFunction())
      {
        callback = arguments[1];
      }
      else if (arguments[0]->IsString() && arguments[1]->IsString() && arguments[2]->IsFunction())
      {
        params = arguments[1]->GetStringValue();
        callback = arguments[2];
      }
      else
      {
        exception = "Invalid arguments.";
        return false;
      }

      // 执行 C++ 方法
      if (!js_bridge_->CallCppFunction(function_name, params, callback))
      {
        WCHAR msg[128]{};
        wsprintf(msg, L"Failed to call function %s.", function_name.c_str());
        exception = msg;
        return false;
      }

      return true;
    }
    else if (name == "register")
    {
      if (arguments[0]->IsString() && arguments[1]->IsFunction())
      {
        std::string function_name = arguments[0]->GetStringValue();
        CefRefPtr<CefV8Value> callback = arguments[1];
        if (!js_bridge_->RegisterJSFunc(function_name, callback))
        {
          exception = "Failed to register function.";
          return false;
        }
        return true;
      }
      else
      {
        exception = "Invalid arguments.";
        return false;
      }
    }
    else if (name == "unregister")
    {
      if (arguments[0]->IsString())
      {
        std::string function_name = arguments[0]->GetStringValue();
        js_bridge_->UnRegisterJSFunc(function_name, frame);
        return true;
      }
      else
      {
        exception = "Invalid arguments.";
        return false;
      }
    }

    return false;
  }

}
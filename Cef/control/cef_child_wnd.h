#pragma once
#include "cef_control_base.h"
#include "manager/cef_manager.h"
#include "mouse_hook/cef_mouse_hook.h"

class CefChildWnd
  : public CefControlBase
{
public:

  CefChildWnd() = default;

  CefChildWnd(HWND parent_wnd, RECT const& rect)
  {
    CreateWnd(parent_wnd, rect);
  }

  void CreateWnd(HWND parent_wnd, RECT const& rect)
  {
    if (!browser_handler_.get()) {
      parent_wnd_ = parent_wnd;
      rect_ = rect;

      //LONG style = GetWindowLong(parent_wnd_, GWL_STYLE);
      //SetWindowLong(parent_wnd_, GWL_STYLE, style | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

      browser_handler_ = new BrowserHandler;
      browser_handler_->SetHandlerDelegate(this);
      browser_handler_->SetHostWindow(parent_wnd_);

      //注册到鼠标钩子处理链
      CefMouseHook::Instance()->AddCefControlBase(this);

      js_bridge_.reset(new CefJSBridge);

      ReCreateBrowser();
    }
  }

  virtual ~CefChildWnd()
  {
    DettachDevTools();

    CefMouseHook::Instance()->RemoveCefControlBase(this);

    if (browser_handler_.get() && browser_handler_->GetBrowser().get())
    {
      // Request that the main browser close.
      browser_handler_->CloseAllBrowser();
      browser_handler_->SetHostWindow(NULL);
      browser_handler_->SetHandlerDelegate(NULL);
    }
  }

  void SetVisible(bool is_visible)
  {
    HWND hwnd = GetCefHandle();
    if (hwnd)
    {
      if (is_visible)
      {
        ShowWindow(hwnd, SW_SHOW);
      } else
      {
        SetWindowPos(hwnd, NULL, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOACTIVATE);
      }
    }
  }

  void ReCreateBrowser() override
  {
    if (browser_handler_->GetBrowser() == nullptr)
    {
      CefWindowInfo window_info;
      window_info.SetAsChild(parent_wnd_, rect_);

      CefBrowserSettings browser_settings;
      CefBrowserHost::CreateBrowser(window_info, browser_handler_, url_.c_str(), browser_settings, NULL);
    }
  }
protected:
  HWND parent_wnd_;
  RECT rect_;
};


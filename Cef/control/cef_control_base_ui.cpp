#include "cef_control_base.h"
#include "include/cef_browser.h"
#include "include/cef_frame.h"
#include "handler/browser_handler.h"
#include "manager/cef_manager.h"
#include "app/cef_js_bridge.h"


CefControlBase::CefControlBase(void)
{
  devtool_attached_ = false;
}

CefControlBase::~CefControlBase(void)
{

}
void CefControlBase::LoadURL(const CefString& url)
{
  if (browser_handler_.get() && browser_handler_->GetBrowser().get())
  {
    CefRefPtr<CefFrame> frame = browser_handler_->GetBrowser()->GetMainFrame();
    if (!frame)
      return;

    frame->LoadURL(url);
  } else
  {
    if (browser_handler_.get())
    {
      browser_handler_->AddAfterCreateTask([this, url]() {LoadURL(url); });
    }
  }
}

void CefControlBase::GoBack()
{
  if (browser_handler_.get() && browser_handler_->GetBrowser().get())
  {
    return browser_handler_->GetBrowser()->GoBack();
  }
}

void CefControlBase::GoForward()
{
  if (browser_handler_.get() && browser_handler_->GetBrowser().get())
  {
    return browser_handler_->GetBrowser()->GoForward();
  }
}

bool CefControlBase::CanGoBack()
{
  if (browser_handler_.get() && browser_handler_->GetBrowser().get())
  {
    return browser_handler_->GetBrowser()->CanGoBack();
  }
  return false;
}

bool CefControlBase::CanGoForward()
{
  if (browser_handler_.get() && browser_handler_->GetBrowser().get())
  {
    return browser_handler_->GetBrowser()->CanGoForward();
  }
  return false;
}

void CefControlBase::Refresh()
{
  if (browser_handler_.get() && browser_handler_->GetBrowser().get())
  {
    return browser_handler_->GetBrowser()->Reload();
  }
}

void CefControlBase::StopLoad()
{
  if (browser_handler_.get() && browser_handler_->GetBrowser().get())
  {
    return browser_handler_->GetBrowser()->StopLoad();
  }
}

bool CefControlBase::IsLoading()
{
  if (browser_handler_.get() && browser_handler_->GetBrowser().get())
  {
    return browser_handler_->GetBrowser()->IsLoading();
  }
  return false;
}

void CefControlBase::StartDownload(const CefString& url)
{
  if (browser_handler_.get() && browser_handler_->GetBrowser().get())
  {
    browser_handler_->GetBrowser()->GetHost()->StartDownload(url);
  }
}

void CefControlBase::SetZoomLevel(float zoom_level)
{
  if (browser_handler_.get() && browser_handler_->GetBrowser().get())
  {
    browser_handler_->GetBrowser()->GetHost()->SetZoomLevel(zoom_level);
  }
}

HWND CefControlBase::GetCefHandle() const
{
  if (browser_handler_.get() && browser_handler_->GetBrowserHost().get())
    return browser_handler_->GetBrowserHost()->GetWindowHandle();

  return NULL;
}

HWND CefControlBase::GetHostWindow() const
{
  return browser_handler_ ? browser_handler_->GetHostWindow() : NULL;
}

CefString CefControlBase::GetURL()
{
  if (browser_handler_.get() && browser_handler_->GetBrowser().get())
  {
    return browser_handler_->GetBrowser()->GetMainFrame()->GetURL();
  }

  return CefString();
}

std::string CefControlBase::GetUTF8URL()
{
  if (browser_handler_.get() && browser_handler_->GetBrowser().get())
  {
    return helper_lib::ToString(GetURL().c_str(), CP_UTF8);
  }

  return {};
}

CefString CefControlBase::GetMainURL(const CefString& url)
{
  std::string temp = url.ToString();
  int end_pos = temp.find("#") == std::string::npos ? temp.length() : temp.find("#");
  temp = temp.substr(0, end_pos);
  return CefString(temp.c_str());
}

bool CefControlBase::RegisterCppFunc(const std::wstring& function_name, CppFunction function, bool global_function/* = false*/)
{
  if (!browser_handler_.get() || !js_bridge_.get()) return false;
  if (browser_handler_->GetBrowser().get())
  {
    return js_bridge_->RegisterCppFunc(helper_lib::ToString(function_name, CP_UTF8).c_str(), function, global_function ? nullptr : browser_handler_->GetBrowser());
  } else
  {
    browser_handler_->AddAfterCreateTask([=]() { RegisterCppFunc(function_name, function, global_function); });
  }

  return true;
}

void CefControlBase::UnRegisterCppFunc(const std::wstring& function_name)
{
  if (browser_handler_.get() && browser_handler_->GetBrowser().get() && js_bridge_.get())
  {
    js_bridge_->UnRegisterCppFunc(helper_lib::ToString(function_name, CP_UTF8).c_str(), browser_handler_->GetBrowser());
  }
}

bool CefControlBase::CallJSFunction(const std::wstring& js_function_name, const std::wstring& params, CallJsFunctionCallback callback, const std::wstring& frame_name /*= L""*/)
{
  if (browser_handler_.get() && browser_handler_->GetBrowser().get() && js_bridge_.get())
  {
    CefRefPtr<CefFrame> frame = frame_name == L"" ?
      browser_handler_->GetBrowser()->GetMainFrame() :
      browser_handler_->GetBrowser()->GetFrame(frame_name);

    if (!js_bridge_->CallJSFunction(helper_lib::ToString(js_function_name, CP_UTF8).c_str(),
      helper_lib::ToString(params, CP_UTF8).c_str(), frame, callback))
    {
      return false;
    }

    return true;
  }

  return false;
}

bool CefControlBase::CallJSFunction(const std::wstring& js_function_name, const std::wstring& params, CallJsFunctionCallback callback, int frame_id)
{
  if (browser_handler_.get() && browser_handler_->GetBrowser().get() && js_bridge_.get())
  {
    CefRefPtr<CefFrame> frame = browser_handler_->GetBrowser()->GetFrame(frame_id);
    frame = frame ? frame : browser_handler_->GetBrowser()->GetMainFrame();

    if (!js_bridge_->CallJSFunction(helper_lib::ToString(js_function_name, CP_UTF8).c_str(),
      helper_lib::ToString(params, CP_UTF8).c_str(), frame, callback))
    {
      return false;
    }

    return true;
  }

  return false;
}

void CefControlBase::RepairBrowser()
{
  ReCreateBrowser();
}

namespace
{
  class DevToolsHandler
    :public CefClient
    , public CefLifeSpanHandler
  {

  public:
    OnBeforeCloseEvent before_close_event_;
  protected:
    ///
    // Return the handler for browser life span events.
    ///
    /*--cef()--*/
    virtual CefRefPtr<CefLifeSpanHandler> GetLifeSpanHandler() { return this; }

    ///
    // Called just before a browser is destroyed. Release all references to the
    // browser object and do not attempt to execute any methods on the browser
    // object after this callback returns. This callback will be the last
    // notification that references |browser|. See DoClose() documentation for
    // additional usage information.
    ///
    /*--cef()--*/
    virtual void OnBeforeClose(CefRefPtr<CefBrowser> browser)
    {
      CefManager::PostTaskUIThread([=] {
        if (before_close_event_) {
          before_close_event_(browser);
        }
      });
    }

    IMPLEMENT_REFCOUNTING(DevToolsHandler);
  };
}

bool CefControlBase::AttachDevTools()
{
  if (devtool_attached_)
    return true;


  auto browser = browser_handler_->GetBrowser();
  if (browser == nullptr)
  {
    browser_handler_->AddAfterCreateTask([this]() {CefManager::PostTaskUIThread([this]() {AttachDevTools(); }); });
  } else
  {
    CefWindowInfo windowInfo;
    windowInfo.SetAsPopup(NULL, L"cef_devtools");
    CefBrowserSettings settings;
    windowInfo.width = 900;
    windowInfo.height = 700;
    auto dev_tools_handler = new DevToolsHandler;
    dev_tools_handler->before_close_event_ = [=](auto const&) {
      if (devtool_attached_) {
        devtool_attached_ = false;
      }
      if (cb_devtool_visible_change_ != nullptr) {
        cb_devtool_visible_change_(devtool_attached_);
      }
    };


    browser->GetHost()->ShowDevTools(windowInfo, dev_tools_handler, settings, CefPoint());
    devtool_attached_ = true;
    if (cb_devtool_visible_change_ != nullptr)
      cb_devtool_visible_change_(devtool_attached_);
  }
  return true;
}

void CefControlBase::DettachDevTools()
{
  if (!devtool_attached_)
    return;
  auto browser = browser_handler_->GetBrowser();
  if (browser != nullptr)
  {
    browser->GetHost()->CloseDevTools();
    devtool_attached_ = false;
    if (cb_devtool_visible_change_ != nullptr)
      cb_devtool_visible_change_(devtool_attached_);
  }
}
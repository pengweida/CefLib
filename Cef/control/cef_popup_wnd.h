#pragma once
#include "cef_control_base.h"
#include "manager/cef_manager.h"
#include "mouse_hook/cef_mouse_hook.h"

class CefPopupWnd
  :public CefControlBase
  ,public helper_lib::DragWindowMoveAdapterT<CefPopupWnd>
{
public:
  CefPopupWnd() = default;

  CefPopupWnd(HWND parent_wnd, RECT const& rect, int round_corner = 0,bool init_visible = false) 
  {
    CreateWnd(parent_wnd, rect, round_corner,init_visible);
  }

  void CreateWnd(HWND parent_wnd, RECT const& rect, int round_corner = 0,bool init_visible = false)
  {
    if (!browser_handler_.get()) {
      parent_wnd_ = parent_wnd;
      rect_ = rect;
      round_corner_ = round_corner;
      init_visible_ = init_visible;

      browser_handler_ = new BrowserHandler;
      browser_handler_->SetHandlerDelegate(this);

      browser_handler_->SetIsVisible(init_visible_);

      //注册到鼠标钩子处理链
      CefMouseHook::Instance()->AddCefControlBase(this);

      js_bridge_.reset(new CefJSBridge);

      ReCreateBrowser();
    }
  }

  virtual ~CefPopupWnd()
  {
    DettachDevTools();

    CefMouseHook::Instance()->RemoveCefControlBase(this);

    if (browser_handler_.get() && browser_handler_->GetBrowser().get())
    {
      // Request that the main browser close.
      browser_handler_->CloseAllBrowser();
      browser_handler_->SetHostWindow(NULL);
      browser_handler_->SetHandlerDelegate(NULL);
    }
  }

  void SetVisible(bool is_visible)
  {
    if (!browser_handler_.get() || !js_bridge_.get()) return;
    if (browser_handler_->GetBrowser().get()) {
      browser_handler_->SetIsVisible(is_visible);
      HWND hwnd = GetCefHandle();
      if (hwnd) {
        if (is_visible) {
          ShowWindow(hwnd, SW_SHOW);
        } else {
          SetWindowPos(hwnd, NULL, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOACTIVATE);
        }
      }
    } else {
      browser_handler_->AddAfterCreateTask([=]() { SetVisible(is_visible); });
    }
  }

  HWND GetHWND() const { return GetCefHandle(); }

  void EnableDrag() { AttachDragStart([=] {SetEnterDragMode(false); }); }
protected:
  void ReCreateBrowser() override
  {
    if (browser_handler_->GetBrowser() == nullptr)
    {
      CefWindowInfo window_info;
      window_info.SetAsPopup(parent_wnd_, L"");
      window_info.x = rect_.left;
      window_info.y = rect_.top;
      window_info.width = rect_.right - rect_.left;
      window_info.height = rect_.bottom - rect_.top;
      window_info.style = WS_POPUP | (init_visible_ ? WS_VISIBLE : 0);

      CefBrowserSettings browser_settings;
      CefBrowserHost::CreateBrowser(window_info, browser_handler_, url_.c_str(), browser_settings, NULL);
    }
  }

  virtual bool ShouldCaptureMouseWhenLButtonDown() const override { return false; }

  virtual void OnDragEvent(UINT msg_id, HWND wnd, POINT pt_window) override
  {
    ::ScreenToClient(GetCefHandle(), &pt_window);
    HandleDragMessage(msg_id, -1, (LPARAM)MAKELONG(pt_window.x, pt_window.y));
  }

  virtual bool OnAfterCreated(CefRefPtr<CefBrowser> browser) override
  {
    if (round_corner_ > 0) {
      HWND wnd = GetHWND();
      if (wnd) {
        RECT rect{};
        ::GetClientRect(wnd, &rect);
        HRGN rgn = ::CreateRoundRectRgn(rect.left, rect.top, rect.right, rect.bottom, round_corner_, round_corner_);
        ::SetWindowRgn(wnd, rgn, TRUE);
        ::DeleteObject(rgn);
      }
    }
    return __super::OnAfterCreated(browser);
  }

protected:
  HWND parent_wnd_;
  RECT rect_;
  int round_corner_{0};
  bool init_visible_{ false };
};
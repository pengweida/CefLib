#pragma once
#include "duilib_header.h"
#include "control/cef_control_base.h"

class CefNativeControl :public CefControlBase, public CControlUI
{
public:
  CefNativeControl() = default;

  ~CefNativeControl();
protected:

  /// 重写父类接口，提供个性化功能
  virtual void DoInit() override;
  virtual void SetPos(RECT rc, bool bNeedInvalidate = true) override;
  virtual void DoEvent(TEventUI& event) override;
  virtual void SetVisible(bool bVisible = true) override;
  virtual void SetInternVisible(bool bVisible = true) override;
  virtual void SetManager(CPaintManagerUI* pManager, CControlUI* pParent, bool bInit = true) override;

  //ui线程 用与浏览器对象创建完毕后调整上层窗口大小
  virtual void UpdateWindowPos() OVERRIDE;
protected:
  virtual void ReCreateBrowser() override;
private:

};
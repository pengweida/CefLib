#include "cef_native_control.h"
#include "mouse_hook/cef_mouse_hook.h"


CefNativeControl::~CefNativeControl()
{
  CefMouseHook::Instance()->RemoveCefControlBase(this);
  if (browser_handler_.get() && browser_handler_->GetBrowser().get())
  {
    // Request that the main browser close.
    browser_handler_->CloseAllBrowser();
    browser_handler_->SetHostWindow(NULL);
    browser_handler_->SetHandlerDelegate(NULL);
  }
}

void CefNativeControl::DoInit()
{
  if (browser_handler_.get() == nullptr)
  {
    LONG style = ::GetWindowLong(m_pManager->GetPaintWindow(), GWL_STYLE);
    ::SetWindowLong(m_pManager->GetPaintWindow(), GWL_STYLE, style | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);
    ASSERT((GetWindowExStyle(m_pManager->GetPaintWindow()) & WS_EX_LAYERED) == 0 && L"无法在分层窗口内使用本控件");

    browser_handler_ = new BrowserHandler;
    browser_handler_->SetHostWindow(m_pManager->GetPaintWindow());
    browser_handler_->SetHandlerDelegate(this);

    //注册到鼠标钩子处理链
    CefMouseHook::Instance()->AddCefControlBase(this);

    ReCreateBrowser();
  }

  if (!js_bridge_.get())
  {
    js_bridge_.reset(new CefJSBridge);
  }

  return __super::DoInit();
}

void CefNativeControl::SetPos(RECT rc, bool bNeedInvalidate /*= true*/)
{
  __super::SetPos(rc);

  HWND hwnd = GetCefHandle();
  if (hwnd) {
    CDuiRect rect = rc;
    SetWindowPos(hwnd, NULL, rect.left, rect.top, rect.GetWidth(), rect.GetHeight(), SWP_NOZORDER);
  }
}

void CefNativeControl::DoEvent(TEventUI& event)
{
  if (browser_handler_.get() && browser_handler_->GetBrowser().get() == NULL)
    return __super::DoEvent(event);
  else if (event.Type == UIEVENT_SETFOCUS) {
    if (browser_handler_->GetBrowserHost().get()) {
      browser_handler_->GetBrowserHost()->SetFocus(true);
    }
  } else if (event.Type == UIEVENT_KILLFOCUS) {
    if (browser_handler_->GetBrowserHost().get()) {
      browser_handler_->GetBrowserHost()->SetFocus(false);
    }
  }

  return __super::DoEvent(event);
}

void CefNativeControl::SetVisible(bool bVisible /*= true*/)
{
  __super::SetVisible(bVisible);

  HWND hwnd = GetCefHandle();
  if (hwnd) {
    if (bVisible) {
      ShowWindow(hwnd, SW_SHOW);
    } else {
      SetWindowPos(hwnd, NULL, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOACTIVATE);
    }
  }
}

void CefNativeControl::SetInternVisible(bool bVisible /*= true*/)
{
  __super::SetVisible(bVisible);

  HWND hwnd = GetCefHandle();
  if (hwnd) {
    if (bVisible) {
      ShowWindow(hwnd, SW_SHOW);
    } else {
      SetWindowPos(hwnd, NULL, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOACTIVATE);
    }
  }
}

void CefNativeControl::SetManager(CPaintManagerUI* pManager, CControlUI* pParent, bool bInit /*= true*/)
{
  __super::SetManager(pManager, pParent, bInit);
  if (pManager && m_pManager) {
    if (browser_handler_) {
      browser_handler_->SetHostWindow(pManager->GetPaintWindow());
    }

    // 设置Cef窗口句柄为新的主窗口的子窗口
    auto hwnd = GetCefHandle();
    if (hwnd) {
      ::SetParent(hwnd, pManager->GetPaintWindow());
    }

    // 为新的主窗口重新设置WS_CLIPSIBLINGS、WS_CLIPCHILDREN样式，否则Cef窗口刷新会出问题
    LONG style = GetWindowLong(pManager->GetPaintWindow(), GWL_STYLE);
    SetWindowLong(pManager->GetPaintWindow(), GWL_STYLE, style | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);
  }
}

void CefNativeControl::UpdateWindowPos()
{
  SetPos(m_rcItem);
}

void CefNativeControl::ReCreateBrowser()
{
  if (browser_handler_->GetBrowser() == nullptr)
  {
    // 使用有窗模式
    CefWindowInfo window_info;
    window_info.SetAsChild(m_pManager->GetPaintWindow(), m_rcItem);

    CefBrowserSettings browser_settings;
    CefBrowserHost::CreateBrowser(window_info, browser_handler_, L"", browser_settings, NULL);
  }
}

#pragma once
#include <Windows.h>
#include <windowsx.h>
#include <list>
#include <strsafe.h>
#include <assert.h>

//Cef告诉像素数据 ，绘制到win32窗口 

struct DRAW_DATA
{
  HWND    hwnd_;
  HDC     mem_dc_;
  HBITMAP bitmap_;
  PVOID   bits_;
  int     width_;
  int     height_;

  explicit DRAW_DATA() :hwnd_(NULL), mem_dc_(NULL), bitmap_(NULL), bits_(NULL), width_(0), height_(0)
  {
  }

  ~DRAW_DATA()
  {
    ResetBitmap();
    ResetMemDC();
  }

  void ResetMemDC()
  {
    if (mem_dc_)
    {
      ::DeleteDC(mem_dc_);
      mem_dc_ = NULL;
    }
  }

  void ResetBitmap()
  {
    if (bitmap_)
    {
      ::DeleteObject(bitmap_);
      bitmap_ = NULL;
      bits_ = NULL;
      width_ = height_ = 0;
    }
  }
};


inline HRESULT Draw(__inout DRAW_DATA &drawStruct,
  const void* buffer, int width, int height)
{
  if (!drawStruct.hwnd_ || !buffer || !width || !height)
    return E_INVALIDARG;

  POINT ptRaw = { 0, 0 };
  SIZE  sizeRaw = { width, height };

  HDC hDC = ::GetDC(drawStruct.hwnd_);
  if (!hDC)
    return E_HANDLE;

  if (!drawStruct.mem_dc_)
  {
    drawStruct.mem_dc_ = ::CreateCompatibleDC(hDC);
    if (!drawStruct.mem_dc_)
    {
      ::ReleaseDC(drawStruct.hwnd_, hDC);
      return E_HANDLE;
    }
  }

  if (!drawStruct.bitmap_ || (drawStruct.width_ != width) || (drawStruct.height_ != height))
  {
    drawStruct.ResetBitmap();

    BITMAPINFO bitmapinfo;
    bitmapinfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
    bitmapinfo.bmiHeader.biBitCount = 32;
    bitmapinfo.bmiHeader.biHeight = height;
    bitmapinfo.bmiHeader.biWidth = width;
    bitmapinfo.bmiHeader.biPlanes = 1;
    bitmapinfo.bmiHeader.biCompression = BI_RGB;
    bitmapinfo.bmiHeader.biXPelsPerMeter = 0;
    bitmapinfo.bmiHeader.biYPelsPerMeter = 0;
    bitmapinfo.bmiHeader.biClrUsed = 0;
    bitmapinfo.bmiHeader.biClrImportant = 0;
    bitmapinfo.bmiHeader.biSizeImage = bitmapinfo.bmiHeader.biWidth
      * bitmapinfo.bmiHeader.biHeight * bitmapinfo.bmiHeader.biBitCount / 8;

    drawStruct.bits_ = NULL;
    drawStruct.bitmap_ = ::CreateDIBSection(drawStruct.mem_dc_, &bitmapinfo, 0, &drawStruct.bits_, 0, 0);
    if (!drawStruct.bitmap_ || !drawStruct.bits_)
    {
      ::ReleaseDC(drawStruct.hwnd_, hDC);
      return E_HANDLE;
    }

    drawStruct.width_ = width;
    drawStruct.height_ = height;
    CHECK(drawStruct.bitmap_ && drawStruct.bits_);
  }

  const DWORD dwWidthBytes = sizeof(COLORREF)*width;
  PBYTE pBytesDst = (PBYTE)drawStruct.bits_;
  PBYTE pBytesSrc = (PBYTE)buffer + (height - 1)*dwWidthBytes;
  for (int i = 0; i < height; ++i)
  {
    CopyMemory(pBytesDst, pBytesSrc, dwWidthBytes);
    pBytesDst += dwWidthBytes;
    pBytesSrc -= dwWidthBytes;
  }


  ::SelectObject(drawStruct.mem_dc_, drawStruct.bitmap_);

  BLENDFUNCTION blendFunc32bpp;
  blendFunc32bpp.AlphaFormat = AC_SRC_ALPHA;
  blendFunc32bpp.BlendFlags = 0;
  blendFunc32bpp.BlendOp = AC_SRC_OVER;
  blendFunc32bpp.SourceConstantAlpha = 0xFF;


  ::UpdateLayeredWindow(drawStruct.hwnd_, hDC, NULL, &sizeRaw, drawStruct.mem_dc_, &ptRaw, 0, &blendFunc32bpp, ULW_ALPHA);


  ::ReleaseDC(drawStruct.hwnd_, hDC);

  return S_OK;
}